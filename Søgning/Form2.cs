﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Søgning
{
    public partial class Form2 : Form
    {
        public static string PosOfExportFile;
        public Form2()
        {
            InitializeComponent();
        }

        private void btnMakeFile_Click(object sender, EventArgs e)
        {

            SaveFileDialog saveFileDialog = new SaveFileDialog();

            saveFileDialog.Filter = "txt File|*.txt";
            saveFileDialog.Title = "Choose Where to save your export data";


            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                PosOfExportFile = saveFileDialog.FileName;
                
            }
        }
        private void btnChooseFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "txt File|*.txt";
            if (openFileDialog.ShowDialog() == DialogResult.OK) 
            {
                PosOfExportFile = openFileDialog.FileName;
            }

        }

        private void btnCloseSettings_Click(object sender, EventArgs e)
        {


            this.Close();
        }


    }
}
