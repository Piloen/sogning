﻿namespace Søgning
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstbox1 = new System.Windows.Forms.ListBox();
            this.btn1 = new System.Windows.Forms.Button();
            this.lstbox2 = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btn2 = new System.Windows.Forms.Button();
            this.updown1 = new System.Windows.Forms.NumericUpDown();
            this.lblIterationer = new System.Windows.Forms.Label();
            this.lblTidstagning = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbtnMerge = new System.Windows.Forms.RadioButton();
            this.rbtnBubble = new System.Windows.Forms.RadioButton();
            this.button1 = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.updown1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lstbox1
            // 
            this.lstbox1.FormattingEnabled = true;
            this.lstbox1.ItemHeight = 16;
            this.lstbox1.Location = new System.Drawing.Point(12, 44);
            this.lstbox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lstbox1.Name = "lstbox1";
            this.lstbox1.Size = new System.Drawing.Size(120, 452);
            this.lstbox1.TabIndex = 0;
            // 
            // btn1
            // 
            this.btn1.Location = new System.Drawing.Point(139, 94);
            this.btn1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(415, 46);
            this.btn1.TabIndex = 1;
            this.btn1.Text = "Generate random numbers";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.Btn1_Click);
            // 
            // lstbox2
            // 
            this.lstbox2.FormattingEnabled = true;
            this.lstbox2.ItemHeight = 16;
            this.lstbox2.Location = new System.Drawing.Point(559, 46);
            this.lstbox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lstbox2.Name = "lstbox2";
            this.lstbox2.Size = new System.Drawing.Size(120, 452);
            this.lstbox2.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Unsorted numbers";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(569, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Sorted numbers";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(139, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(131, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Amount of numbers";
            // 
            // btn2
            // 
            this.btn2.Location = new System.Drawing.Point(139, 450);
            this.btn2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(415, 46);
            this.btn2.TabIndex = 7;
            this.btn2.Text = "Sort numbers";
            this.btn2.UseVisualStyleBackColor = true;
            this.btn2.Click += new System.EventHandler(this.Btn2_Click);
            // 
            // updown1
            // 
            this.updown1.Location = new System.Drawing.Point(139, 66);
            this.updown1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.updown1.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.updown1.Name = "updown1";
            this.updown1.Size = new System.Drawing.Size(415, 22);
            this.updown1.TabIndex = 8;
            // 
            // lblIterationer
            // 
            this.lblIterationer.AutoSize = true;
            this.lblIterationer.Location = new System.Drawing.Point(139, 414);
            this.lblIterationer.Name = "lblIterationer";
            this.lblIterationer.Size = new System.Drawing.Size(76, 17);
            this.lblIterationer.TabIndex = 9;
            this.lblIterationer.Text = "Iterationer:";
            // 
            // lblTidstagning
            // 
            this.lblTidstagning.AutoSize = true;
            this.lblTidstagning.Location = new System.Drawing.Point(139, 431);
            this.lblTidstagning.Name = "lblTidstagning";
            this.lblTidstagning.Size = new System.Drawing.Size(36, 17);
            this.lblTidstagning.TabIndex = 10;
            this.lblTidstagning.Text = "Tid: ";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbtnMerge);
            this.groupBox1.Controls.Add(this.rbtnBubble);
            this.groupBox1.Location = new System.Drawing.Point(139, 311);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(415, 100);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Algorithms";
            // 
            // rbtnMerge
            // 
            this.rbtnMerge.AutoSize = true;
            this.rbtnMerge.Location = new System.Drawing.Point(7, 62);
            this.rbtnMerge.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rbtnMerge.Name = "rbtnMerge";
            this.rbtnMerge.Size = new System.Drawing.Size(95, 21);
            this.rbtnMerge.TabIndex = 1;
            this.rbtnMerge.TabStop = true;
            this.rbtnMerge.Text = "MergeSort";
            this.rbtnMerge.UseVisualStyleBackColor = true;
            // 
            // rbtnBubble
            // 
            this.rbtnBubble.AutoSize = true;
            this.rbtnBubble.Location = new System.Drawing.Point(7, 34);
            this.rbtnBubble.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rbtnBubble.Name = "rbtnBubble";
            this.rbtnBubble.Size = new System.Drawing.Size(99, 21);
            this.rbtnBubble.TabIndex = 0;
            this.rbtnBubble.TabStop = true;
            this.rbtnBubble.Text = "BubbleSort";
            this.rbtnBubble.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(139, 144);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(415, 46);
            this.button1.TabIndex = 12;
            this.button1.Text = "Open your own .txt file instead";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(139, 261);
            this.btnExport.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(415, 46);
            this.btnExport.TabIndex = 13;
            this.btnExport.Text = "Data export options";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(695, 510);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblTidstagning);
            this.Controls.Add(this.lblIterationer);
            this.Controls.Add(this.updown1);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lstbox2);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.lstbox1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.updown1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstbox1;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.ListBox lstbox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.NumericUpDown updown1;
        private System.Windows.Forms.Label lblIterationer;
        private System.Windows.Forms.Label lblTidstagning;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbtnMerge;
        private System.Windows.Forms.RadioButton rbtnBubble;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnExport;
    }
}

