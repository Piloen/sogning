﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Søgning
{
    public partial class DataForm : Form
    {

        public List<int> iterations= new List<int>();
        public List<int> timers = new List<int>();
        public DataForm()
        {
            InitializeComponent();
        }

        private void DataForm_Load(object sender, EventArgs e)
        {
            DataChart.ChartAreas["Data"].AxisX.Maximum = findHighestNumber(iterations);
            DataChart.ChartAreas["Data"].AxisX.Interval = findHighestNumber(iterations)/10;
            DataChart.ChartAreas["Data"].AxisY.Maximum = findHighestNumber(timers);
            DataChart.ChartAreas["Data"].AxisY.Interval = findHighestNumber(timers)/10;
            for (int  i = 0;  i <iterations.Count;  i++)
            {
                DataChart.Series["Data"].Points.AddXY(iterations[i],timers[i]);

            }
           
        }
        public void TransferData(List<int> tempIterations, List<int> tempTime)
        {
            iterations = tempIterations;
            timers = tempTime;

        }
        int findHighestNumber(List<int> tempList)
        {

            int maxNumber = int.MinValue;
            foreach (int i in tempList)
            {
                if (i > maxNumber)
                {
                    maxNumber = i;
                }
            }
            return maxNumber;
        }
    }
}
