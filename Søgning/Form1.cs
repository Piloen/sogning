﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace Søgning
{
    public partial class Form1 : Form
    {
        OpenFileDialog openFileDialog = new OpenFileDialog();
        string openedFile;
        List<int> openedFileIntList = new List<int>();

        List<int> iterations = new List<int>();
        List<int> timers = new List<int>();



        Stopwatch myTimer = new Stopwatch();
        public static int iterationer;
        Random random = new System.Random();
        List<int> Numbers = new List<int>();
        public Form1()
        {
            InitializeComponent();
            rbtnBubble.Checked = true;
        }

        private void Btn1_Click(object sender, EventArgs e)
        {
            Numbers.Clear();
            lstbox1.Items.Clear();
            for (int i = 0; i < int.Parse(updown1.Text); i++)
            {
                int genNumber = random.Next(0, 100);
                lstbox1.Items.Add(genNumber);
                Numbers.Add(genNumber);
            }
        }

        private void Btn2_Click(object sender, EventArgs e)
        {
            iterationer = 0;
            myTimer.Start();
            lstbox2.Items.Clear();
            if (rbtnBubble.Checked)
            {
                for (int i = 0; i < Numbers.Count; i++)
                {
                    for (int j = 0; j < Numbers.Count - 1; j++)
                    {
                        if (Numbers[j] > Numbers[j + 1])
                        {
                            int tmp = Numbers[j];
                            Numbers[j] = Numbers[j + 1];
                            Numbers[j + 1] = tmp;
                            iterationer++;
                        }
                    }
                }

                lblIterationer.Text = "Iterationer: " + iterationer.ToString();
                myTimer.Stop();
                lblTidstagning.Text = "Tid: " + myTimer.ElapsedMilliseconds + "ms";

                iterations.Add(iterationer);
                timers.Add(int.Parse(myTimer.ElapsedMilliseconds.ToString()));

                myTimer.Reset();

                foreach (int item in Numbers)
                {
                    lstbox2.Items.Add(item);
                }
            }
            else if (rbtnMerge.Checked)
            {
                List<int> sorted = new List<int>();

                sorted = MergeSort(Numbers);

                lblIterationer.Text = "Iterationer: " + iterationer.ToString();
                myTimer.Stop();
                lblTidstagning.Text = "Tid: " + myTimer.ElapsedMilliseconds + "ms";

                iterations.Add(iterationer);
                timers.Add(int.Parse(myTimer.ElapsedMilliseconds.ToString()));

                myTimer.Reset();

                foreach (int i in sorted)
                {
                    lstbox2.Items.Add(i.ToString());
                }
            }





        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Numbers.Clear();
            openFileDialog.Filter = "txt File|*.txt";
            if (openFileDialog.ShowDialog()==DialogResult.OK)
            {
                openedFile = openFileDialog.FileName;

                System.IO.StreamReader streamReader = new System.IO.StreamReader(openedFile);

                for (int i = 0; i < TotalLines(openedFile); i++)
                {
                    Numbers.Add(int.Parse(streamReader.ReadLine()));
                }
                foreach (int i in Numbers)
                {
                    lstbox1.Items.Add(i);
                }

                streamReader.Close();
            }

        }
        int TotalLines(string filePath)
        {
            using (System.IO.StreamReader r = new System.IO.StreamReader(filePath))
            {
                int i = 0;
                while (r.ReadLine() != null) { i++; }
                return i;
            }
        }
        private static List<int> MergeSort(List<int> unsorted)
        {
            if (unsorted.Count<=1)
                return unsorted;

            List<int> left = new List<int>();
            List<int> right = new List<int>();

            int middle = unsorted.Count / 2;

            for (int i = 0; i < middle; i++)
            {
                left.Add(unsorted[i]);
            }
            for (int i = middle; i < unsorted.Count; i++)
            {
                right.Add(unsorted[i]);
            }

            left = MergeSort(left);
            right = MergeSort(right);

            return Merge(left, right);

        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            /*
            Form2 form2 = new Form2();

            form2.ShowDialog();
            */
            

            DataForm dataForm = new DataForm();
            dataForm.TransferData(iterations, timers);
            dataForm.ShowDialog();
        }

        private static List<int> Merge(List<int> left, List<int> right)
        {
            List<int> result = new List<int>();

            while (left.Count > 0 || right.Count > 0)
            {
                if (left.Count > 0 && right.Count > 0)
                {
                    if (left.First() <= right.First())  //Comparing First two elements to see which is smaller
                    {
                        result.Add(left.First());
                        left.Remove(left.First());      //Rest of the list minus the first element
                    }
                    else
                    {
                        result.Add(right.First());
                        right.Remove(right.First());
                    }
                }
                else if (left.Count > 0)
                {
                    result.Add(left.First());
                    left.Remove(left.First());
                }
                else if (right.Count > 0)
                {
                    result.Add(right.First());

                    right.Remove(right.First());
                }
                iterationer++;
            }
            return result;
        }

    }
}
